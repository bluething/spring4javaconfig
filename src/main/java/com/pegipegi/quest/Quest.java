/**
 * 
 */
package com.pegipegi.quest;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
public interface Quest {
	
	public void embark();
	
}
