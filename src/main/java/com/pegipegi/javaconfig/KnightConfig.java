/**
 * 
 */
package com.pegipegi.javaconfig;

import java.io.PrintStream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pegipegi.knights.DamselRescuingKnight;
import com.pegipegi.knights.Knight;
import com.pegipegi.quest.Quest;
import com.pegipegi.quest.SlayDragonQuest;

/**
 * @author habib.gunadarma@gmail.com
 *
 */
@Configuration
public class KnightConfig {

	@Bean
	public Knight knight() {
		return new DamselRescuingKnight(quest());
	}
	
	@Bean
	public Quest quest() {
		return new SlayDragonQuest(stream());
	}
	
	@Bean
	public PrintStream stream() {
		return System.out;
	}
	
}
