package com.pegipegi;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.pegipegi.javaconfig.KnightConfig;
import com.pegipegi.knights.Knight;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(KnightConfig.class);
        Knight knight = context.getBean(Knight.class);
        knight.embarkOnQuest();
        context.close();
    }
}
